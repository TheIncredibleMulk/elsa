package panelpoll

import "encoding/xml"

// generated from https://www.onlinetool.io/xmltogo/
type SMARTSWITCH2 struct {
	XMLName xml.Name `xml:"SMARTSWITCH2"`
	Text    string   `xml:",chardata"`
	CABINET struct {
		Text                string `xml:",chardata"`
		VERSION             string `xml:"VERSION,attr"`             // "3.1.3.7"
		NUM                 string `xml:"NUM,attr"`                 // "1"
		NAME                string `xml:"NAME,attr"`                // "My ERP"
		RACKTYPE            string `xml:"RACKTYPE,attr"`            // "ERP-FT"
		STATION             string `xml:"STATION,attr"`             // "DISABLED"
		DMX                 string `xml:"DMX,attr"`                 // "ENABLED"
		DMXPRI              string `xml:"DMXPRI,attr"`              // "100"
		PRESETPRI           string `xml:"PRESETPRI,attr"`           // "100"
		CIPRI               string `xml:"CIPRI,attr"`               // "100"
		PACKETDELAY         string `xml:"PACKETDELAY,attr"`         // "0"
		DMXLOSSMODE         string `xml:"DMXLOSSMODE,attr"`         // "HOLD LAST LOOK"
		DMXWAITTIME         string `xml:"DMXWAITTIME,attr"`         // "180"
		DMXFADETIME         string `xml:"DMXFADETIME,attr"`         // "3"
		SACNLOSSMODE        string `xml:"SACNLOSSMODE,attr"`        // "HOLD LAST LOOK"
		SACNWAITTIME        string `xml:"SACNWAITTIME,attr"`        // "180"
		SACNFADETIME        string `xml:"SACNFADETIME,attr"`        // "3"
		POWERON             string `xml:"POWERON,attr"`             // "LAST-LOOK"
		RELAYDELAY          string `xml:"RELAYDELAY,attr"`          // "0"
		LLONRANGE           string `xml:"LLONRANGE,attr"`           // "81% TO 90%"
		LLOFFRANGE          string `xml:"LLOFFRANGE,attr"`          //  "41% TO 50%"
		LLONTIME            string `xml:"LLONTIME,attr"`            // "5"
		LLOFFTIME           string `xml:"LLOFFTIME,attr"`           // "5"
		REMOTERECORD        string `xml:"REMOTERECORD,attr"`        // "ENABLED"
		WEBINTERFACEENABLED string `xml:"WEBINTERFACEENABLED,attr"` // "ENABLED"
		CID                 string `xml:"CID,attr"`                 // "30477AD0-F672-37B4-B161-F84D9A131893"
		DCID                string `xml:"DCID,attr"`                //"5C3F3D33-9E18-402F-B4F6-F6A3114F5BCB"
	} `xml:"CABINET"`
	RELAY []struct {
		Text      string `xml:",chardata"`
		NUMBER    string `xml:"NUMBER,attr"`
		UDN       string `xml:"UDN,attr"`
		DMX       string `xml:"DMX,attr"`
		UNIV      string `xml:"UNIV,attr"`
		SACNADDR  string `xml:"SACNADDR,attr"`
		SPACE     string `xml:"SPACE,attr"`
		ZONE      string `xml:"ZONE,attr"`
		TYPE      string `xml:"TYPE,attr"`
		OPMODE    string `xml:"OPMODE,attr"`
		ONTHRESH  string `xml:"ONTHRESH,attr"`
		OFFTHRESH string `xml:"OFFTHRESH,attr"`
		PANIC     string `xml:"PANIC,attr"`
		ALLOWMAN  string `xml:"ALLOWMAN,attr"`
		ANMODE    string `xml:"ANMODE,attr"`
		NAME      string `xml:"NAME,attr"`
	} `xml:"RELAY"`
	UNIVERSE []struct {
		Text       string `xml:",chardata"`
		UNIVINRACK string `xml:"UNIVINRACK,attr"`
		NUMBER     string `xml:"NUMBER,attr"`
	} `xml:"UNIVERSE"`
	SPACE []struct {
		Text           string `xml:",chardata"`
		SPACEINRACK    string `xml:"SPACEINRACK,attr"`
		NUMBER         string `xml:"NUMBER,attr"`
		NAME           string `xml:"NAME,attr"`
		SPACEINRACKEXT string `xml:"SPACEINRACKEXT,attr"`
		NUMBEREXT      string `xml:"NUMBEREXT,attr"`
		NAMEEXT        string `xml:"NAMEEXT,attr"`
	} `xml:"SPACE"`
	PRESET []struct {
		Text   string `xml:",chardata"`
		NUMBER string `xml:"NUMBER,attr"`
	} `xml:"PRESET"`
	PREFADELEVEL []struct {
		Text        string `xml:",chardata"`
		SPACEINRACK string `xml:"SPACEINRACK,attr"`
		UPTIME      string `xml:"UPTIME,attr"`
	} `xml:"PREFADELEVEL"`
	PRELEVEL []struct {
		Text  string `xml:",chardata"`
		RELAY string `xml:"RELAY,attr"`
		LEVEL string `xml:"LEVEL,attr"`
	} `xml:"PRELEVEL"`
	SEQUENCE struct {
		Text   string `xml:",chardata"`
		NUMBER string `xml:"NUMBER,attr"`
		TYPE   string `xml:"TYPE,attr"`
	} `xml:"SEQUENCE"`
	STEP []struct {
		Text    string `xml:",chardata"`
		STEPNUM string `xml:"STEPNUM,attr"`
		USED    string `xml:"USED,attr"`
	} `xml:"STEP"`
	SEQTIME []struct {
		Text     string `xml:",chardata"`
		SPACE    string `xml:"SPACE,attr"`
		UPTIME   string `xml:"UPTIME,attr"`
		HOLDTIME string `xml:"HOLDTIME,attr"`
	} `xml:"SEQTIME"`
	SEQLEVEL []struct {
		Text  string `xml:",chardata"`
		RELAY string `xml:"RELAY,attr"`
		LEVEL string `xml:"LEVEL,attr"`
	} `xml:"SEQLEVEL"`
	CONTACTIN []struct {
		Text           string `xml:",chardata"`
		CONTACTNUM     string `xml:"CONTACTNUM,attr"`
		EVALONBOOT     string `xml:"EVALONBOOT,attr"`
		CONTACTSPACE   string `xml:"CONTACTSPACE,attr"`
		CIOPENACTION   string `xml:"CIOPENACTION,attr"`
		OPENPARAMONE   string `xml:"OPENPARAMONE,attr"`
		OPENPARAMTWO   string `xml:"OPENPARAMTWO,attr"`
		CICLOSEDACTION string `xml:"CICLOSEDACTION,attr"`
		CLOSEDPARAMONE string `xml:"CLOSEDPARAMONE,attr"`
		CLOSEDPARAMTWO string `xml:"CLOSEDPARAMTWO,attr"`
	} `xml:"CONTACTIN"`
	PANIC struct {
		Text     string `xml:",chardata"`
		INDELAY  string `xml:"INDELAY,attr"`
		OUTDELAY string `xml:"OUTDELAY,attr"`
		SHEDLOAD string `xml:"SHEDLOAD,attr"`
	} `xml:"PANIC"`
	PANLEVEL []struct { // PANLEVEL = PANIC LEVEL aka the level the relay will load shed to.
		Text  string `xml:",chardata"`
		RELAY string `xml:"RELAY,attr"`
		LEVEL string `xml:"LEVEL,attr"`
	} `xml:"PANLEVEL"`
	NETWORK struct {
		Text        string `xml:",chardata"`
		IP          string `xml:"IP,attr"`
		SUBNET      string `xml:"SUBNET,attr"`
		GATEWAY     string `xml:"GATEWAY,attr"`
		IPTYPE      string `xml:"IPTYPE,attr"`
		SERVEREN    string `xml:"SERVEREN,attr"`
		SERVERIP    string `xml:"SERVERIP,attr"`
		FTPUSER     string `xml:"FTPUSER,attr"`
		FTPPASS     string `xml:"FTPPASS,attr"`
		CONFIGPATH  string `xml:"CONFIGPATH,attr"`
		UPGRADEPATH string `xml:"UPGRADEPATH,attr"`
	} `xml:"NETWORK"`
	CURRENTNETWORK struct {
		Text    string `xml:",chardata"`
		IP      string `xml:"IP,attr"`
		SUBNET  string `xml:"SUBNET,attr"`
		GATEWAY string `xml:"GATEWAY,attr"`
	} `xml:"CURRENTNETWORK"`
	EVENTS []struct {
		Text        string `xml:",chardata"`
		EVENTNUM    string `xml:"EVENTNUM,attr"`
		ACTION      string `xml:"ACTION,attr"`
		SPACE       string `xml:"SPACE,attr"`
		ACTIONINDEX string `xml:"ACTIONINDEX,attr"`
		RECURRENCE  string `xml:"RECURRENCE,attr"`
		WHEN        string `xml:"WHEN,attr"`
		HOURS       string `xml:"HOURS,attr"`
		MIN         string `xml:"MIN,attr"`
		MODE        string `xml:"MODE,attr"`
		AUTOHOLD    string `xml:"AUTOHOLD,attr"`
	} `xml:"EVENTS"`
	TIMECLOCK struct {
		Text          string `xml:",chardata"`
		ENABLED       string `xml:"ENABLED,attr"`
		TIMESTYLE     string `xml:"TIMESTYLE,attr"`
		HOLDTIME      string `xml:"HOLDTIME,attr"`
		HOLIDAYTIME   string `xml:"HOLIDAYTIME,attr"`
		FLICKWARN     string `xml:"FLICKWARN,attr"`
		FLICKWARNTIME string `xml:"FLICKWARNTIME,attr"`
		TIMEZONE      string `xml:"TIMEZONE,attr"`
		LAT           string `xml:"LAT,attr"`
		LONG          string `xml:"LONG,attr"`
	} `xml:"TIMECLOCK"`
	DST struct {
		Text          string `xml:",chardata"`
		DSTMODE       string `xml:"DSTMODE,attr"`
		DSTSTARTMONTH string `xml:"DSTSTARTMONTH,attr"`
		DSTSTARTDOTW  string `xml:"DSTSTARTDOTW,attr"`
		DSTSTARTOCCUR string `xml:"DSTSTARTOCCUR,attr"`
		DSTENDMONTH   string `xml:"DSTENDMONTH,attr"`
		DSTENDDOTW    string `xml:"DSTENDDOTW,attr"`
		DSTENDOCCUR   string `xml:"DSTENDOCCUR,attr"`
	} `xml:"DST"`
	PREFS struct {
		Text   string `xml:",chardata"`
		BLMODE string `xml:"BLMODE,attr"`
		BLTIME string `xml:"BLTIME,attr"`
		LANG   string `xml:"LANG,attr"`
	} `xml:"PREFS"`
	HOLIDAYSHUTOFF []struct {
		Text   string `xml:",chardata"`
		SPACE  string `xml:"SPACE,attr"`
		ACTION string `xml:"ACTION,attr"`
	} `xml:"HOLIDAY_SHUTOFF"`
	DIMMER []struct {
		Text     string `xml:",chardata"`
		NUMBER   string `xml:"NUMBER,attr"`
		MINSCALE string `xml:"MINSCALE,attr"`
	} `xml:"DIMMER"`
	OPEN struct {
		Text          string `xml:",chardata"`
		WEEKDAYSHOUR  string `xml:"WEEKDAYSHOUR,attr"`
		WEEKDAYSMIN   string `xml:"WEEKDAYSMIN,attr"`
		SATURDAYSHOUR string `xml:"SATURDAYSHOUR,attr"`
		SATURDAYSMIN  string `xml:"SATURDAYSMIN,attr"`
		SUNDAYSHOUR   string `xml:"SUNDAYSHOUR,attr"`
		SUNDAYSMIN    string `xml:"SUNDAYSMIN,attr"`
	} `xml:"OPEN"`
	CLOSED struct {
		Text          string `xml:",chardata"`
		WEEKDAYSHOUR  string `xml:"WEEKDAYSHOUR,attr"`
		WEEKDAYSMIN   string `xml:"WEEKDAYSMIN,attr"`
		SATURDAYSHOUR string `xml:"SATURDAYSHOUR,attr"`
		SATURDAYSMIN  string `xml:"SATURDAYSMIN,attr"`
		SUNDAYSHOUR   string `xml:"SUNDAYSHOUR,attr"`
		SUNDAYSMIN    string `xml:"SUNDAYSMIN,attr"`
	} `xml:"CLOSED"`
	SACNHLL struct {
		Text   string `xml:",chardata"`
		TIMEMS string `xml:"TIMEMS,attr"`
	} `xml:"SACNHLL"`
	SYSLOG struct {
		Text   string `xml:",chardata"`
		IPADDR string `xml:"IPADDR,attr"`
	} `xml:"SYSLOG"`
}
