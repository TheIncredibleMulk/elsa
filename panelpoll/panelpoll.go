package panelpoll

import (
	"bufio"
	"crypto/tls"
	"encoding/xml"
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/shiena/ansicolor"
	log "github.com/sirupsen/logrus"
	"gitlab.com/TheIncredibleMulk/elsa/srvloc"
)

// <---------------------------------  HTTP Client Configuration options for the file --------------------------------->
// custom http client because go will timeout forever if the server (aka panel) is no longer avaliable
var timeout = time.Second * 5
var client = &http.Client{Transport: &http.Transport{TLSClientConfig: &tls.Config{InsecureSkipVerify: true}}, CheckRedirect: nil, Timeout: timeout}

// <------------------------------------------------------------------------------------------------------------------->

// <---------------------------------------------  Models for XML Reply  --------------------------------------------->
// Panel holds the data necessary to poll for data
type Panel struct {
	ip          net.IP
	kind        any
	description string
	address     string
	request     string
}

// ERP2 is the XML reply wrapper to get_relay_info
type ERPxml struct {
	XMLName xml.Name `xml:"html"`
	Info    struct { // ERPInformation is the XML reply wrapper to get_relay_info
		XMLName  xml.Name `xml:"information"`
		Circuits []struct {
			XMLName     xml.Name `xml:"prop_info"`
			Slot        int16    `xml:"slot,attr"`
			Breakertype int8     `xml:"type,attr"`
			Density     int8     `xml:"density,attr"`
			Breaker     string   `xml:"breaker,attr"`
			Output      string   `xml:"output,attr"`
			Load        float32  `xml:"load,attr"`
			Power       float32  `xml:"power,attr"`
			Level       string   `xml:"level,attr"`
			Source      string   `xml:"src,attr"`
			Circuit     int16    `xml:"circuit,attr"`
			Space       int8     `xml:"space,attr"`
		} `xml:"prop_info"` // EchoRelayInfo is the XML reply to get_relay_info
	} `xml:"information"`
}
type ERP2 struct {
	XMLName     xml.Name `xml:"html"`
	Text        string   `xml:",chardata"`
	Information struct {
		Text     string `xml:",chardata"`
		PropInfo []struct {
			Text    string `xml:",chardata"`
			Slot    string `xml:"slot,attr"`
			Type    string `xml:"type,attr"`
			Density string `xml:"density,attr"`
			Breaker string `xml:"breaker,attr"`
			Output  string `xml:"output,attr"`
			Load    string `xml:"load,attr"`
			Power   string `xml:"power,attr"`
			Level   string `xml:"level,attr"`
			Src     string `xml:"src,attr"`
			Circuit string `xml:"circuit,attr"`
			Space   string `xml:"space,attr"`
		} `xml:"prop_info"`
	} `xml:"information"`
}

// CEM is the XML reply wrapper to get_relay_info
type CEM struct {
	XMLName xml.Name `xml:"html"`
	Info    struct { // CEMInformation is the XML reply wrapper to get_relay_info
		XMLName xml.Name   `xml:"information"`
		CEMInfo []struct { // CEMInfo is the XML reply to get_relay_info
			XMLName xml.Name `xml:"info"`
			UDN     int16    `xml:"udn,attr"`
			Circuit int8     `xml:"circuit,attr"`
			Space   int8     `xml:"space,attr"`
			Side    string   `xml:"side,attr"`
			Wsource string   `xml:"wsource,attr"`
			Level   int8     `xml:"level,attr"`
		} `xml:"info"`
	} `xml:"information"`
}

// <------------------------------------------------------------------------------------------------------------------->

var paneltypes = []Panel{
	Panel{
		description: "ERP",
		address:     "http://10.101.100.101/action.cgi",
		request:     "<setlevels><get_relay_info slot=\"all\"/></setlevels>",
	},
	Panel{
		description: "CEM Levels",
		address:     "http://10.101.101.101/goform/dimmertest",
		request:     "<setlevels><get udn=\"all\"/></setlevels>",
	},
	Panel{
		description: "CEM Setup",
		address:     "http://10.101.101.101/goform/dimmertest",
		request:     "<setlevels><get_prop udn=\"all\"/></setlevels",
	},
	Panel{
		description: "CEM Spaces",
		address:     "http://10.101.101.101/goform/dimmertest",
		request:     "<setlevels><get_space_info/></setlevels",
	},
}

/*
<----------------------------------------------------------------- Functions that make life better ----------------------------------------------------------------->
*/

// initLogger is used to make powershell print out colors correctly
func initLogger() {
	log.SetFormatter(&log.TextFormatter{
		ForceColors: true,
	})
	log.SetOutput(ansicolor.NewAnsiColorWriter(os.Stdout))
}

func fck(err error) error {
	if err != nil {
		log.Errorln("Error :", err)
		return err
	}
	return nil
}

func readint(prompt string) int {
	stdin := bufio.NewReader(os.Stdin)
	log.Println(prompt)
	var input int
	for {
		_, err := fmt.Fscan(stdin, &input)
		if err == nil && input >= 0 {
			break
		}
		stdin.ReadString('\n')
		log.Errorln("Sorry, invalid input. Please enter a non-negative integer: ")
	}
	return input
}

/*
<----------------------------------------------------------------- The base getRequests because the defaults are missing things we need (like timeouts) ----------------------------------------------------------------->
*/

func getRequest(address string, body io.Reader) (_ []byte, err error) {
	initLogger()

	req, err := http.NewRequest(http.MethodGet, address, nil)
	if err != nil {
		log.Errorln("[GetRequest] Error! - http Get error: ", err)
		return nil, err
	}
	req.Header.Add("accept", "application/xml;")

	log.Println("sending : ", req.Method, " at ", req.URL)
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	if os.IsTimeout(err) {
		log.Errorf("[GetRequest] timeout error: (%T) %v", err, err)
		return nil, err
	}
	if resp != nil {
		body, _ := ioutil.ReadAll(resp.Body)
		// log.Printf("Response Code [%v] Body: \n%s\n", resp.Status, body)
		defer resp.Body.Close()
		return body, nil
	}
	return nil, fmt.Errorf("[getRequest] Error")
}

func postRequest(address string, body io.Reader) (_ []byte, err error) {
	initLogger()

	req, err := http.NewRequest(http.MethodPost, address, body)
	if err != nil {
		log.Errorln("[postRequest] Error! - http Get error: ", err)
		return nil, err
	}
	req.Header.Add("accept", "application/xml;")

	// log.Println("sending : ", req.Method, " at ", req.URL)

	resp, err := client.Do(req)
	if err != nil {
		log.Errorf("[postRequest] undefined client error: (%T) %v", err, err)
		return nil, err
	}
	if os.IsTimeout(err) {
		log.Errorf("[postRequest] timeout error: (%T) %v", err, err)
		return nil, err
	}
	if resp != nil {
		body, _ := ioutil.ReadAll(resp.Body)
		// log.Printf("Response Code [%v] Body: \n%s\n", resp.Status, body)
		defer resp.Body.Close()
		return body, nil
	}
	if resp.StatusCode >= 200 && resp.StatusCode <= 299 {
		return nil, nil
	} else {
		return nil, fmt.Errorf("[postRequest] Error. [", resp.StatusCode, "] ", resp.Status)
	}
}

/*
<----------------------------------------------------------------- CME3 Requests for data ----------------------------------------------------------------->
*/

func GetCEM3Levels(acnERP srvloc.ACNComponent) (circuits CEM, err error) {
	body := strings.NewReader("<setlevels><get udn=\"all\"/></setlevels>")
	b, err := postRequest("http://"+acnERP.Addr+"/goform/dimmertest", body)
	if err != nil {
		return circuits, err
	} else {
		err = xml.Unmarshal(b, &circuits)
		if err != nil {
			return circuits, err
		} else {
			// log.Printf("System UnMarshalled: \n %+v", circuits)
			return circuits, nil
		}
	}
}

// ReleaseERPLevels accepts an ACN
func ReleaseCEM3Levels(acnERP srvloc.ACNComponent, space int16) (circuits ERPxml, err error) {
	body := strings.NewReader(fmt.Sprintf("<release_all space=\"%v\"/>", space))
	b, err := postRequest("http://"+acnERP.Addr+"/goform/dimmertest", body)
	if err != nil {
		return circuits, err
	} else {
		err = xml.Unmarshal(b, &circuits)
		if err != nil {
			return circuits, err
		} else {
			// log.Printf("System UnMarshalled: \n %+v", circuits)
			return circuits, nil
		}
	}
}

/*
<----------------------------------------------------------------- ERP and SensorIQ Requests ----------------------------------------------------------------->
*/

func GetERPConfig(acnERP srvloc.ACNComponent) (erpConfig SMARTSWITCH2, err error) {
	resp, err := postRequest("http://"+acnERP.Addr+"GetConfig", nil)
	if err != nil {
		return erpConfig, err
	}
	_, loc, found := strings.Cut(string(resp), ":\\")
	if found != false {
		return erpConfig, fmt.Errorf("didn't find a ':\\' to split the GetConfig Response.")
	}
	b, err := postRequest("http://"+acnERP.Addr+"/ramdisk/"+loc, nil)
	if err != nil {
		return erpConfig, err
	} else {
		err = xml.Unmarshal(b, &erpConfig)
		if err != nil {
			return erpConfig, err
		} else {
			// log.Printf("System UnMarshalled: \n %+v", erpConfig)
			return erpConfig, nil
		}
	}
}

func GetERPLevels(acnERP srvloc.ACNComponent) (circuits ERPxml, err error) {
	body := strings.NewReader("<setlevels><get_relay_info slot=\"all\"/></setlevels>")
	b, err := postRequest("http://"+acnERP.Addr+"/action.cgi", body)
	// log.Warnln("xmlReply:\n %s\n", b)
	if err != nil {
		return circuits, err
	} else {
		err = xml.Unmarshal(b, &circuits)
		if err != nil {
			return circuits, err
		} else {
			// log.Printf("System UnMarshalled: \n %+v", circuits)
			return circuits, nil
		}
	}
}

func GetERPSpaces(acnERP srvloc.ACNComponent) (circuits ERPxml, err error) {
	body := strings.NewReader("<setlevels><get_space_info slot=\"all\"/></setlevels>")
	b, err := postRequest("http://"+acnERP.Addr+"/action.cgi", body)
	log.Warnln("xmlReply:\n %s\n", b)
	if err != nil {
		return circuits, err
	} else {
		err = xml.Unmarshal(b, &circuits)
		if err != nil {
			return circuits, err
		} else {
			// log.Printf("System UnMarshalled: \n %+v", circuits)
			return circuits, nil
		}
	}
}

func GetERPRack(acnERP srvloc.ACNComponent) (circuits ERPxml, err error) {
	body := strings.NewReader("<setlevels><get_rack_info slot=\"all\"/></setlevels>")
	b, err := postRequest("http://"+acnERP.Addr+"/action.cgi", body)
	log.Warnln("xmlReply:\n %s\n", b)
	if err != nil {
		return circuits, err
	} else {
		err = xml.Unmarshal(b, &circuits)
		if err != nil {
			return circuits, err
		} else {
			// log.Printf("System UnMarshalled: \n %+v", circuits)
			return circuits, nil
		}
	}
}

// GetERPPowerReading accepts an ACNComponent and returns a csv of error strings we put them in an array and return the array
func GetERPPowerReading(acnERP srvloc.ACNComponent) (circuits ERPxml, err error) {
	body := strings.NewReader("<setlevels><get_relay_info slot=\"all\"/></setlevels>")
	b, err := postRequest("http://"+acnERP.Addr+"/PowerReading.cgi", body)
	log.Warnln("xmlReply:\n %s\n", b)
	if err != nil {
		return circuits, err
	} else {
		err = xml.Unmarshal(b, &circuits)
		if err != nil {
			return circuits, err
		} else {
			// log.Printf("System UnMarshalled: \n %+v", circuits)
			return circuits, nil
		}
	}
}

// GetERPErrors accepts an ACNComponent and returns a csv of error strings we put them in an array and return the array
func GetERPErrors(acnERP srvloc.ACNComponent) (erpErrors []string, err error) {
	// body := strings.NewReader("")
	b, err := getRequest("http://"+acnERP.Addr+"/GetErrors.cgi", nil)
	if err != nil {
		return erpErrors, err
	} else {
		erpErrors = strings.Split(string(b), ",")
		return erpErrors, err
	}

}

// ReleaseERPLevels accepts an ACN
func SetERPLevels(acnERP srvloc.ACNComponent, space int16, circuit int16, level int16) (circuits ERPxml, err error) {
	body := strings.NewReader(fmt.Sprintf("<setlevels><udn=\"%d\" space=\"%v\" level=\"%d\"/></setlevels>", circuit, space, level))
	b, err := postRequest("http://"+acnERP.Addr+"/action.cgi", body)
	if err != nil {
		return circuits, err
	} else {
		err = xml.Unmarshal(b, &circuits)
		if err != nil {
			return circuits, err
		} else {
			// log.Printf("System UnMarshalled: \n %+v", circuits)
			return circuits, nil
		}
	}
}

// ReleaseERPLevels accepts an ACNComponent and returns the state of the levels in the panel
func ReleaseERPLevels(acnERP srvloc.ACNComponent, space int16) (circuits ERPxml, err error) {
	body := strings.NewReader(fmt.Sprintf("<setlevels><release_all space=\"%v\"/></setlevels>", space))
	b, err := postRequest("http://"+acnERP.Addr+"/action.cgi", body)
	if err != nil {
		// log.Errorln("Release ERP Levels Error: ", err)
		return circuits, err
	} else {
		err = xml.Unmarshal(b, &circuits)
		if err != nil {
			return circuits, err
		} else {
			// log.Printf("System UnMarshalled: \n %+v", circuits)
			return circuits, nil
		}
	}
}

func main() {
	// 	for k, v := range paneltypes {
	// 		fmt.Printf("%d for %s\n", k, v.description)
	// 	}

	// 	var choice int = readint("Enter choice: ")

	// 	// query paneltype
	// 	log.Println("Request data: ")
	// 	// log.Println(requestBody[choice])

	// 	resp, err := client.Post(paneltypes[choice].address, "application/xml", strings.NewReader(paneltypes[choice].request))
	// 	if err != nil {
	// 		log.Fatalln(err)
	// 	}

	// 	defer resp.Body.Close()

	// 	body, err := ioutil.ReadAll(resp.Body)
	// 	if err != nil {vert the DDL instructions to Go Struct to convert the s
	// 		log.Fatalln(err)
	// 	}

	// 	log.Println("Response: ")
	// 	log.Println(string(body))

	// 	if choice == 0 {
	// 		// ERP
	// 		var html ERP
	// 		err := xml.Unmarshal(body, &html)
	// 		if err != nil {
	// 			log.Printf("Conversion to xml failed %s\n", err)
	// 		}
	// 		if err == nil {
	// 			// display sorted?
	// 			//log.Println(html)
	// 			for _, info := range html.Info[0].EchoRelayInfo {
	// 				panel := "ERP"
	// 				// sqlStatement := `
	// 				// INSERT INTO circuits (panel_slot,panel,slot,circuit,breakertype,density,breaker,source,output,load,power,level,space)
	// 				// VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13)
	// 				// ON CONFLICT (panel_slot) DO UPDATE
	// 				// SET
	// 				// circuit=$4,
	// 				// breakertype=$5,
	// 				// density=$6,v
	// 				// breaker=$7,
	// 				// source=$8,
	// 				// output=$9,
	// 				// load=$10,
	// 				// power=$11,
	// 				// level=$12,
	// 				// space=$13,
	// 				// response_timestamp=CURRENT_TIMESTAMP
	// 				// `

	// 				keyval := fmt.Sprintf("%s-%03d", panel, info.Slot)
	// 				// _, err := db.Exec(sqlStatement, keyval, panel, info.Slot, info.Circuit, info.Breakertype,
	// 				// 	info.Density, info.Breaker, info.Source, info.Output, info.Load, info.Power, info.Level, info.Space)
	// 				if err == nil {
	// 					log.Printf("Wrote or updated record for %s", keyval)
	// 				} else {
	// 					log.Printf("Failed write for %s error %s", keyval, err)
	// 				}
	// 			}
	// 		}
	// 	}

	// 	if choice == 1 {
	// 		// CEM circuit info
	// 		var html CEM
	// 		log.Println("xml Unmarshal to CEM")
	// 		err := xml.Unmarshal(body, &html)
	// 		if err != nil {
	// 			log.Printf("Conversion to xml failed %s\n", err)
	// 		}
	// 		if err == nil {
	// 			// display sorted?
	// 			for _, info := range html.Info[0].CEMInfo {
	// 				panel := "DR1"
	// 				// sqlStatement := `
	// 				// INSERT INTO circuits (panel_slot,panel,slot,circuit,breakertype,density,breaker,source,output,load,power,level,space)
	// 				// VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13)
	// 				// ON CONFLICT (panel_slot) DO UPDATE
	// 				// SET
	// 				// circuit=$4,
	// 				// breakertype=$5,
	// 				// density=$6,
	// 				// breaker=$7,
	// 				// source=$8,
	// 				// output=$9,
	// 				// load=$10,
	// 				// power=$11,
	// 				// level=$12,
	// 				// space=$13,
	// 				// response_timestamp=CURRENT_TIMESTAMP
	// 				// `

	// 				keyval := fmt.Sprintf("%s-%03d", panel, info.Circuit)
	// 				log.Println(keyval + " : " + info.Wsource)
	// 				// _, err := db.Exec(sqlStatement, keyval, panel, info.Circuit, info.UDN, 0,
	// 				// 0, "NA", info.Wsource, "NA", 0, 0, info.Level, info.Space)
	// 				if err == nil {
	// 					log.Printf("Wrote or updated record for %s", keyval)
	// 				} else {
	// 					log.Printf("Failed write for %s error %s", keyval, err)
	// 				}
	// 			}
	// 		}
	// 	}

}
