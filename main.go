package main

import (
	"fmt"
	"net"
	"os"
	"strconv"
	"time"

	"github.com/shiena/ansicolor"
	log "github.com/sirupsen/logrus"
	"gitlab.com/TheIncredibleMulk/elsa/panelpoll"
	"gitlab.com/TheIncredibleMulk/elsa/srvloc"
	"gitlab.com/TheIncredibleMulk/elsa/tui"
)

const (
	erpDCID = "5c3f3d33-9e18-402f-b4f6-f6a3114f5bcb"
	erpURI  = "/action.cgi"

	cem3DCID    = "D63B23B8-6407-4E1A-973D-796D777A64B9"
	oldCem3DCID = "BF6E6FE9-7A07-424C-8640-7B86D14BAA6E"
	cem3URI     = "goform/dimmertest"

	eacpDCID = "5c3f3d33-9e18-402f-b4f6-f6a3114f5bcb"
	eacp

	erpMk2DCID
	erp
)

type Panel struct {
	Component srvloc.ACNComponent
	Model     string
	Name      string
	Ip        net.IP
	Circuts   []circuit
}

type circuit struct {
	CktId    int
	SetLevel bool
	Space    int8
	Level    int8
	Slot     int16
}

// initLogger is used to make powershell print out colors correctly
func initLogger() {
	log.SetFormatter(&log.TextFormatter{
		ForceColors: true,
	})
	log.SetOutput(ansicolor.NewAnsiColorWriter(os.Stdout))
}

// reads in an []net.Addr from net.Interface.Addrs() strips anything that is not IPv4 and returns a single net.IP
// we discovered that depending on the OS and speed of the processor the resulting []net.IP does not have a consistent return of IPv4 or IPv6 in the return array
func returnOnlyIPv4(ip []net.Addr) (ipv4 net.IP) {
	initLogger()
	for k, _ := range ip {
		ip, _, _ := net.ParseCIDR(ip[k].String())
		// log.Println("ip:", ip)
		if ip.To4() != nil {
			ipv4 = ip
		}
	}
	return ipv4
}

func listInterfaces(list []net.Interface) {
	for i, iface := range list {
		addrs, err := iface.Addrs()
		if err != nil {
			panic(err)
		}
		fmt.Printf("%d %s\t%v\n", i, iface.Name, addrs)
		IPAddrs := []net.Addr{}
		for _, addr := range addrs {
			//log.Printf(" %d %v\n", j, addr)
			IPAddrs = append(IPAddrs, addr)
		}
	}
}

func panelParse(x srvloc.ACNComponent) (p Panel) {
	// log.Printf("\n%+v", x)
	switch x.DCID {
	case cem3DCID:
		log.Println("Found an CEM3!")
		p = Panel{
			Component: x,
			Model:     "CEM3",
			Name:      x.FriendlyName,
			Ip:        net.ParseIP(x.Addr),
		}
		// log.Printf("%+v\n", x)

		cem3, err := panelpoll.GetCEM3Levels(x)
		if err != nil {
			log.Panicln(err)
		}
		// log.Printf("%+v", cem3)
		p.Circuts = make([]circuit, len(cem3.Info.CEMInfo))
		for _, cemCir := range cem3.Info.CEMInfo {
			p.Circuts[cemCir.Circuit-1] = circuit{
				Space: cemCir.Space,
				Level: cemCir.Level,
				Slot:  cemCir.UDN,
			}
			if cemCir.Wsource == "setlevel" {
				p.Circuts[cemCir.Circuit-1].SetLevel = true
			} else {
				p.Circuts[cemCir.Circuit-1].SetLevel = false
			}
		}
		// for i, v := range p.Circuts {
		// 	log.Warnf("values of Circuits %v %+v\n", i, v)
		// }
		return
	case oldCem3DCID:
		log.Println("Found an CEM3!")
		p = Panel{
			Component: x,
			Model:     "CEM3",
			Name:      x.FriendlyName,
			Ip:        net.ParseIP(x.Addr),
		}
		// log.Printf("%+v\n", x)

		cem3, err := panelpoll.GetCEM3Levels(x)
		if err != nil {
			log.Panicln(err)
		}
		// log.Printf("%+v", cem3)
		p.Circuts = make([]circuit, len(cem3.Info.CEMInfo))
		for _, cemCir := range cem3.Info.CEMInfo {
			p.Circuts[cemCir.Circuit-1] = circuit{
				Space: cemCir.Space,
				Level: cemCir.Level,
				Slot:  cemCir.UDN,
			}
			if cemCir.Wsource == "setlevel" {
				p.Circuts[cemCir.Circuit-1].SetLevel = true
			} else {
				p.Circuts[cemCir.Circuit-1].SetLevel = false
			}
		}
		// for i, v := range p.Circuts {
		// 	log.Warnf("values of Circuits %v %+v\n", i, v)
		// }
	case erpDCID:
		log.Println("Found an ERP!")
		p = Panel{
			Component: x,
			Model:     "ERP",
			Ip:        net.ParseIP(x.Addr),
		}

		erp, err := panelpoll.GetERPLevels(x)
		if err != nil {
			log.Panicln(err)
		}
		// log.Printf("%+v", erp)
		p.Circuts = make([]circuit, len(erp.Info.Circuits))
		for _, erpCir := range erp.Info.Circuits {
			c := erpCir.Circuit - 1
			// if c < 1 {
			p.Circuts[c] = circuit{
				Space: erpCir.Space,
				Slot:  erpCir.Slot,
			}
			l, err := strconv.Atoi(erpCir.Level)
			if err != nil {
				log.Panicln(err)
			} else {
				p.Circuts[c].Level = int8(l)
			}
			if erpCir.Source == "Set" {
				p.Circuts[c].SetLevel = true
			} else {
				p.Circuts[c].SetLevel = false
			}
			// } else {
			// 	log.Panicf("panel %s : %s [%s] isn't properly configured. Circuit # is less than 1.", p.Model, p.Component.FriendlyName, p.Ip)
		}
		// for i, v := range p.Circuts {
		// 	log.Warnf("values of Circuits %v %+v\n", i, v)
		// }
		return
	default:
		// log.Warnf("ACNComponent is not a Panel: %v", x)
	}
	return
}

func main() {
	initLogger()

	// We begin by discovering all the available network interfaces
	// list, err := net.Interfaces()
	// if err != nil {
	// 	panic(err)
	// }
	//TODO: initialize the ui
	tui.IpSelectStartup()
	// listInterfaces(list)

	// Prompt user to select an interface form the list they were just presented.
	// var netBuff int
	// fmt.Printf("\n>  ")
	// fmt.Scanln(&netBuff)
	// if netBuff > len(list) {
	// 	log.Println("YOOOO, That wasn't an option try again.")
	// 	panic("restart to retry")
	// }

	// Log out the details of the interface chosen
	// log.Printf("You Selected: #%d, %s\n", netBuff, list[netBuff].Name)
	// addrs, err := list[netBuff].Addrs()
	// if err != nil {
	// 	panic(err)
	// }
	// localaddress := returnOnlyIPv4(addrs)

	var Panels []Panel

	// start listening to SRVLOC
	// go srvloc.Listen(localaddress.String())

	// goroutine to manage incoming slp discovery packets and strip them of the unnecessary devices
	go func() {
		for {
			x := <-srvloc.DiscoverChannel
			p := panelParse(x)
			// strip out any non panel devices
			if p.Ip != nil {
				Panels = append(Panels, p)
			}
		}
	}()
	panelSet := false
	time.Sleep(time.Second * 5)
	if len(Panels) > 0 {
		fmt.Printf("\n <-- Wow! We found some panels! --> \n")
		// log.Printf("%+v\n", Panels)
		for _, v := range Panels {
			levelsSet := false
			for _, c := range v.Circuts {
				if c.SetLevel {
					// log.Warnf("Panel %v [%v] has %v's level set", v.Name, v.Ip, i)
					levelsSet = true
					panelSet = true
				}
			}
			if levelsSet {
				log.Printf("Panel: %s [%s] Has Set Levels on Circuits:\n", v.Component.FriendlyName, v.Component.Addr)
				for i, c := range v.Circuts {
					if c.SetLevel {
						fmt.Printf("     Circuit: %v\t Rack Space: %v\t Level: %v\n", i+1, c.Space, c.Level)
					}
				}
			} else {
				log.Printf("Panel: %s [%s] Did not have set levels\n", v.Component.FriendlyName, v.Component.Addr)
			}
		}
	}
	if panelSet {
		// Prompt user for choice about panels and act accordingly
		fmt.Printf("\nWould you like to release the set levels in all panels with set levels?\n")
		var ansBuf string
		fmt.Printf(">  ")
		_, err := fmt.Scanln(&ansBuf)
		if err != nil {
			panic(err)
		}

		switch ansBuf {
		case "Y", "y", "yes", "YES":
			for _, v := range Panels {
				for i, c := range v.Circuts {
					if c.SetLevel {
						// fmt.Printf("     %v\n", i)
						if v.Component.DCID == erpDCID {
							_, err := panelpoll.ReleaseERPLevels(v.Component, int16(c.Space))
							if err != nil {
								log.Errorln("Problem releasing levels", err)
							}
						}
						if v.Component.DCID == cem3DCID {
							_, err := panelpoll.ReleaseCEM3Levels(v.Component, int16(c.Space))
							if err != nil {
								log.Errorln("Problem releasing levels", err)
							}
						}
						// log.Println("Released circuit %v", v.Circuts)
						log.Printf("Releasing Panel %s [%s] Space %v, Circuit %v \n", v.Component.FriendlyName, v.Component.Addr, c.Space, i+1)
					}
				}
			}
			log.Printf("\nOooookkkaaay Lights are released\n")
		case "N", "n", "no", "NO":
			log.Println("Got it, I'll go away now")
			os.Exit(1)
		default:
			log.Println("That's not a valid input please input 'Y' or 'N'")
		}
	} else {
		log.Println("There are not any levels to let go!")
	}

}
