package tui

import (
	"fmt"
	"io"
	"net"
	"os"

	"github.com/charmbracelet/bubbles/list"
	tea "github.com/charmbracelet/bubbletea"
)

type ipSelectModel struct {
	list       list.Model
	lastPress  string
	err        error
	interfaces []net.Interface
	cursor     int
	choice     string
	selected   map[int]struct{}
	quitting   bool
}

func ipSelectInitialModel() ipSelectModel {
	// We begin by discovering all the available network interfaces
	list, err := net.Interfaces()
	if err != nil {
		panic(err)
	}
	return ipSelectModel{
		err:        nil,
		interfaces: list,
		selected:   make(map[int]struct{}),
		quitting:   false,
	}
}

type item string

func (i item) FilterValue() string { return "" }

type itemDelegate struct{}

func (d itemDelegate) Height() int                               { return 1 }
func (d itemDelegate) Spacing() int                              { return 0 }
func (d itemDelegate) Update(msg tea.Msg, m *list.Model) tea.Cmd { return nil }
func (d itemDelegate) Render(w io.Writer, m list.Model, index int, listItem list.Item) {
	i, ok := listItem.(item)
	if !ok {
		return
	}

	str := fmt.Sprintf("%d. %s", index+1, i)

	fn := itemStyle.Render
	if index == m.Index() {
		fn = func(s string) string {
			return selectedItemStyle.Render("> " + s)
		}
	}

	fmt.Fprint(w, fn(str))
}

func (m ipSelectModel) Init() tea.Cmd {
	// Just return `nil`, which means "no I/O right now, please."
	return nil
}

func (m ipSelectModel) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var cmd tea.Cmd

	switch msg := msg.(type) {
	case tea.WindowSizeMsg:
		m.list.SetWidth(msg.Width)
		return m, nil

	case errMsg:
		m.err = msg
		return m, nil

	// Is it a key press?
	case tea.KeyMsg:
		switch keypress := msg.String(); keypress {
		case "ctrl+c":
			m.quitting = true
			return m, tea.Quit

		case "enter":
			i, ok := m.list.SelectedItem().(item)
			if ok {
				m.choice = string(i)
			}
			return m, tea.Quit
		}
		// m.lastPress = msg.String();
		// switch msg.String() {
		// // These keys should exit the program.
		// case "ctrl+c":
		// 	return m, tea.Quit
		// // escape should exit insert mode
		// case "esc":
		// }

		// // Standard List Selection Mode
		// switch msg.String() {
		// // The "up" and "k" keys move the cursor up
		// case "up", "k":
		// 	if m.cursor > 0 {
		// 		m.cursor--
		// 	}
		// // The "down" and "j" keys move the cursor down
		// case "down", "j":
		// 	if m.cursor < len(m.interfaces)-1 {
		// 		m.cursor++
		// 	}
		// // The "enter" key and the spacebar (a literal space) toggles
		// // the selected state for the item that the cursor is pointing at.
		// case "enter", " ":
		// 	_, ok := m.selected[m.cursor]
		// 	if ok {
		// 		delete(m.selected, m.cursor)
		// 	} else {
		// 		m.selected[m.cursor] = struct{}{}
		// 	}
		// 	return m, cmd
		// // These keys should exit the program.
		// case "ctrl+c", "q":
		// 	return m, tea.Quit
		// }
	}
	m.list, cmd = m.list.Update(msg)
	// Return the updated model to the Bubble Tea runtime for processing.
	// Note that we're not returning a command.
	return m, cmd
}

func (m ipSelectModel) View() string {

	if m.choice != "" {
		return quitTextStyle.Render(fmt.Sprintf("%s Sounds good to me.", m.choice))
	}
	if m.quitting {
		return quitTextStyle.Render("Quitting. The cold never bothered me anyway.... ")
	}
	return "\n" + m.list.View()

	// The header
	// s := titleStyle.Render("\n<❄❄❄❄❄❄❄❄❄❄❄❄❄ ⛄ Elsa a program to help your ETC panels \"LET IT GO\" ⛄ ❄❄❄❄❄❄❄❄❄❄❄❄❄>\n")
	// if len(m.interfaces) <= 0 {
	// 	s += readmeStyle.Render("\nSelect the Lighting Network Interface\n\n")
	// }
	// s += foregroundStyle.Render("\nNetwork Interfaces\n")

	// // Iterate over our interfaces
	// if len(m.interfaces) > 0 {
	// 	for i, ifaces := range m.interfaces {

	// 		// Is the cursor pointing at this choice?
	// 		cursor := " " // no cursor
	// 		if m.cursor == i {
	// 			cursor = ">" // cursor!
	// 		}

	// 		// Is this choice selected?
	// 		// checked := " " // not selected
	// 		// if _, ok := m.selected[i]; ok {
	// 		// 	checked = "x" // selected!
	// 		// }

	// 		// Convert iface.Addrs into a field we can print
	// 		addrs, err := ifaces.Addrs()
	// 		if err != nil {
	// 			panic(err)
	// 		}
	// 		// Render the row
	// 		s += fmt.Sprintf("\n%s %d %s\t%v", cursor, i, ifaces.Name, addrs)
	// }
	// }

	// The footer

	// s += helpStyle.Render(fmt.Sprintf("\nm.interfaces: %#v:%+v", m.interfaces, m.interfaces))
	// s += helpStyle.Render(fmt.Sprintf("\nSelected: %+v", m.selected))
	// s += helpStyle.Render("\nLast Key Press: " + m.lastPress)

	// Send the UI for rendering
	// return s
}

func IpSelectStartup() {
	ifaces, err := net.Interfaces()
	if err != nil {
		panic(err)
	}

	var items []list.Item

	for _, v := range ifaces {
		// Convert iface.Addrs into a field we can print
		addrs, err := v.Addrs()
		if err != nil {
			panic(err)
		}
		// todo get an interface number selected based on the iface list
		items = append(items, item(fmt.Sprintf("%s\t%v", v.Name, addrs)))
	}

	const defaultWidth = 20
	const listHeight = 14

	l := list.New(items, itemDelegate{}, defaultWidth, listHeight)
	l.Title = "Select the Lighting Network Interface"
	l.SetShowStatusBar(false)
	l.SetFilteringEnabled(false)
	l.Styles.Title = titleStyle
	l.Styles.PaginationStyle = paginationStyle
	l.Styles.HelpStyle = helpStyle

	m := ipSelectModel{list: l}

	p := tea.NewProgram(m)
	if err := p.Start(); err != nil {
		fmt.Printf("Alas, there's been an error: %v", err)
		os.Exit(1)
	}

}
