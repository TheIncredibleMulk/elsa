package echoPCPmk2

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"log/slog"
	"net"
	"rons-magic-software/models"

	"github.com/wailsapp/wails/v3/pkg/application"
	"gorm.io/gorm"
)

func Monitor(ctx context.Context, iface *net.Interface, echoPCPmk2Multicast net.UDPAddr, location string, db *gorm.DB) {
	// <--	Starting Discovery Server  -> //
	ser, err := net.ListenMulticastUDP("udp", iface, &echoPCPmk2Multicast)
	if err != nil {
		slog.Error("Listening to Multicast UDP", "err", err)
		panic(err)
	}

	packet := make([]byte, 2048)
	for {
		select {
		case <-ctx.Done():
			slog.Warn("Stopping Echo PCP Mk2 Discovery")
			return
		default:
			n, remoteaddr, err := ser.ReadFromUDP(packet)
			// slog.Warn("Received a message", "from", remoteaddr, "message", packet[0:n])
			if err != nil {
				slog.Error("Reading from UDP", "error", err)
				continue
			}
			slog.Debug("Echo PCP Mk2 Message received", "length", n, "addr", remoteaddr)
			if len(packet) < 20 {
				slog.Error("Packet is too short, it cannot be shorter than 20 bytes", "length", len(packet))
				continue
			}
			pcpMk2, err := parseDiscoveryPacket(packet[0:n])
			if err != nil {
				slog.Error("Parse Discovery Packet Error", "error", err)
			}

			// slog.Warn("pcpMK2 Received", "pcpmk2", pcpMk2)
			pcpMk2.Location = location
			results := db.Where("c_id = ?", pcpMk2.CID).Find(&models.EchoPcpMk2{})
			if results.RowsAffected > 0 {
				slog.Info("EchoPcpMk2 Device Found updating", "CID", pcpMk2.CID)
				db.Where("c_id = ?", pcpMk2.CID).Updates(&pcpMk2) // update the device
				continue
			}
			slog.Warn("Creating EchoPcpMk2 Device", "CID", pcpMk2.CID)
			err = pcpMk2.Create(db)
			if err == nil {
				app := application.Get()
				if len(pcpMk2.Tags.Network.Interfaces) > 0 {
					app.EmitEvent("echoPcpMk2-new", fmt.Sprintf("Model: %s IP: %s", pcpMk2.ModelRack, pcpMk2.Tags.Network.Interfaces[0].Ipv4.CurrentAddr))
				}
			}
			if err != nil {
				slog.Error("Creating Echo PCP Mk2", "err", err)
			}
		}
	}
}

// <------------ Loop through packet and assign data into a useful structure  --------------->//
// TODO add error checking to this fuction
func parseDiscoveryPacket(packet []byte) (models.EchoPcpMk2, error) {
	etclinkHeader := []byte{69, 84, 67, 76, 73, 78, 75, 0, 0, 0, 0, 0, 0, 0, 0, 1} // ETCLINK, with padded 0's for a 16 byte header
	incomingHeader := packet[0:16]
	var echoPCPmk2Discovery models.EchoPcpMk2Discovery

	// slog.Warn("Header  Received", "header", incomingHeader)
	// slog.Warn("ETCLINK Header", "header", etclinkHeader)

	if bytes.Equal(etclinkHeader, incomingHeader) { // verify that it is an ETCLINK packet
		packet = packet[16:] // re-slice the incoming packet to remove headers and trailing data that go parses as
		err := json.Unmarshal(packet, &echoPCPmk2Discovery)
		if err != nil {
			slog.Error("Error unmarshalling JSON", "err", err)
			return models.EchoPcpMk2{EchoPcpMk2Discovery: echoPCPmk2Discovery}, err
		}

	} else {
		return models.EchoPcpMk2{}, fmt.Errorf("Not an ETCLINK packet")
	}

	return models.EchoPcpMk2{EchoPcpMk2Discovery: echoPCPmk2Discovery}, nil
}
