package srvloc

import (
	"encoding/hex"
	"fmt"
	"net"
	"os"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/shiena/ansicolor"
	log "github.com/sirupsen/logrus"
)

// initLogger is used to make powershell print out colors correctly
func initLogger() {
	log.SetFormatter(&log.TextFormatter{
		ForceColors: true,
	})
	log.SetOutput(ansicolor.NewAnsiColorWriter(os.Stdout))
}

var ACNComponents = sync.Map{}

var DiscoverChannel = make(chan ACNComponent)

//ACNComponent is a network entity that contains one or more ACN devices
type ACNComponent struct {
	Model        string            `json:"model"`
	FriendlyName string            `json:"friendlyname"`
	Addr         string            `json:"addr"`
	Port         string            `json:"port"`
	DDLFilepath  string            `json:"ddlfilepath"`
	CID          string            `json:"cid"`
	DCID         string            `json:"did"`
	Version      string            `json:"version"`
	FirstSeen    int64             `json:"firstseen"`
	LastSeen     int64             `json:"lastseen"`
	Attributes   map[string]string `json:"raw"`
}

var slpFunctions = map[int]string{
	1: "Service Request",
	2: "Service Reply",
	3: "Service Registration",
	5: "Service Acknowledge",
	6: "Attribute Request",
	7: "Attribute Reply",
	8: "DA Advertisement",
}

func loopSLP(ser *net.UDPConn, localNet net.IP) {
	slponeaddr, err := net.ResolveUDPAddr("udp", "224.0.1.35:427")
	if err != nil {
		log.Panic(err)
	}
	slptwoaddr, err := net.ResolveUDPAddr("udp", "239.255.255.253:427")
	if err != nil {
		log.Panic(err)
	}
	for {
		go sendSLPOne(ser, slponeaddr)           // SLP 1
		go sendSLPTwo(ser, slptwoaddr, localNet) //SLP 2
		time.Sleep(300 * time.Second)
	}
}

func sendSLPOne(conn *net.UDPConn, addr *net.UDPAddr) {
	initLogger()
	time.Sleep(1 * time.Second)
	log.Debug("Sending SLP One\n")
	firstrequest, _ := hex.DecodeString("0201000031200000000066900002656e00000017736572766963653a6469726563746f72792d6167656e74000000000000") // service: directory-agent
	conn.WriteToUDP(firstrequest, addr)
	secondrequest, _ := hex.DecodeString("0201000031200000000066900002656e00000017736572766963653a61636e2e6573746100000000000000000000000000") //service: acn.esta
	conn.WriteToUDP(secondrequest, addr)

}

func sendSLPTwo(conn *net.UDPConn, addr *net.UDPAddr, localIP net.IP) {
	initLogger()
	// localAddress := ""
	log.Debug("Sending SLP Two\n")
	//service: directory agent

	firstrequest, _ := hex.DecodeString("0201000031200000000066900002656e00000017736572766963653a6469726563746f72792d6167656e74000000000000")
	hexAddress := stringHexPadded(fmt.Sprintf("service:directory-agent://%s", localIP), 0)
	addressLen := len(hexAddress) / 2
	packetLen := 42 + addressLen
	secondrequest, _ := hex.DecodeString(fmt.Sprintf("0208%s000000000000000002656e00005d25f5ea00%s%s000b41434e2d44454641554c540000000000", intHexPadded(packetLen, 6), intHexPadded(addressLen, 2), hexAddress))
	// thirdrequest, _ := hex.DecodeString("01005e7ffffd00133b0fac18080045000087226a4000ff11f89a0a6565feeffffffd01ab01ab007360e5020100006b200000000066930002656e003a36332e3233392e31392e3132362c31302e3130312e3130312e3235342c3136392e3235342e3233322e3139312c31302e3130312e31302e31302c0017736572766963653a6469726563746f72792d6167656e74000000000000")
	//service: acn.esta
	//fourthrequest, _ := hex.DecodeString("0201000031200000000066900002656e00000017736572766963653a6469726563746f72792d6167656e74000000000000")
	//fifthrequest, _ := hex.DecodeString("01005e7ffffdc400ad2d172b08004500006b8de64000ff11e9570a3c0a0aeffffffd01ab01ab00578eb60208000052000000000000000002656e00005f7fa37d0028736572766963653a6469726563746f72792d6167656e743a2f2f203136392e3235342e32322e35312e36302e31302e3130000b41434e2d44454641554c540000000000")
	//sixthrequest, _ := hex.DecodeString("01005e7ffffd00133b0fac18080045000087226a4000ff11f89a0a6565feeffffffd01ab01ab007360e5020100006b200000000066930002656e003a35352e3131312e32322e3333332c35352e3130312e3130312e3235342c3136392e3235342e3233322e3139312c35352e3130312e31302e31302c0017736572766963653a61636e2e6573746120202020202020000000000000")
	//need to replace IP in second request, need to do again with service: acn.esta
	conn.WriteToUDP(firstrequest, addr)
	time.Sleep(1 * time.Second)
	conn.WriteToUDP(secondrequest, addr)
	time.Sleep(1 * time.Second)
	// fmt.Printf("Third Request %s", thirdrequest)
	// conn.WriteToUDP(thirdrequest, addr)
}

func listenSRVLOC(ser *net.UDPConn, channel chan []byte) {
	initLogger()
	p := make([]byte, 2048)
	for {
		n, remoteaddr, err := ser.ReadFromUDP(p)
		log.Debugf("Read a message from %v %s \n", remoteaddr, p)

		if err != nil {
			log.Errorf("listenSRVLOC error: %v\n", err)
			continue
		}
		channel <- p[0:n]

	}
}

func hexToInt(data []byte) int {
	length, _ := strconv.ParseInt(hex.EncodeToString(data), 16, 32)
	return int(length)
}

func intHexPadded(data int, length int) string {
	datahex := fmt.Sprintf("%0"+strconv.Itoa(length)+"x", data)
	return datahex
}

func stringHexPadded(data string, length int) string {
	datahex := fmt.Sprintf("%0"+strconv.Itoa(length)+"x", data)
	return datahex
}

func parseSLP(data []byte) {
	initLogger()
	switch hexToInt(data[1:2]) {
	case 1:
		// log.Warnf("Function: Service Request\n%s\n", data)
	case 2:
		// log.Warnf("Function: Service Reply\n%s\n", data)
	case 3: // Service Registration
		// log.Warnf("Function: Service Registration\n%s\n", data)
		serviceurlend := hexToInt(data[19:21]) + 21
		servicetypeend := hexToInt(data[serviceurlend+1:serviceurlend+3]) + serviceurlend + 3
		// serviceurl := fmt.Sprintf("%s", data[21:serviceurlend])
		// servicetype := fmt.Sprintf("%s", data[serviceurlend+3:servicetypeend])
		scopelistend := hexToInt(data[servicetypeend:servicetypeend+2]) + servicetypeend + 2
		// scopelist := fmt.Sprintf("%s", data[servicetypeend+2:scopelistend])
		attrend := hexToInt(data[scopelistend:scopelistend+2]) + scopelistend + 2
		attr := fmt.Sprintf("%s", data[scopelistend+2:attrend])
		// log.WithFields(log.Fields{
		// 	"Packet Description": slpFunctions[hexToInt(data[1:2])],
		// 	"Bytes Long":         hexToInt(data[2:5]),
		// 	"Service URL":        serviceurl,
		// 	"Service Type":       servicetype,
		// 	"Scope List":         scopelist,
		// "Attributes":         attr,
		// }).Info("Packet Data:\n")
		// // fmt.Printf("%s %d bytes long, Service URL: %s, service type: %s Scope List: %s Attributes: %s\n", slpFunctions[hexToInt(data[1:2])], hexToInt(data[2:5]), serviceurl, servicetype, scopelist, attr
		// fmt.Printf("%s  :Service URL: %s Attributes: %s\n", slpFunctions[hexToInt(data[1:2])], serviceurl, attr)
		// s := serviceurl[20 : len(serviceurl)-1]
		// fmt.Println(s)
		st := strings.Split(attr, ",")
		// fmt.Println(st)
		attributes := make(map[string]string)
		for i := range st {
			test := strings.Split(st[i][1:len(st[i])-1], "=")
			if len(test) > 2 {
				attributes[test[0]] = test[1] + "=" + test[2]
				//fmt.Printf("\n\n%s :  %s\n\n", test[0], attributes[test[0]])
			} else if len(test) > 1 { //adding for safety
				attributes[test[0]] = test[1]
				//	fmt.Printf("\n\n%s :  %s\n\n", test[0], test[1])
			} else {
				attributes[test[0]] = ""
			}
			strings.Replace(attributes[test[0]], " ", "", -1)
		}
		dmp := attributes["csl-esta.dmp"]
		dcid := strings.Split(dmp, ":")[2]
		// log.Printf("Device Description: %s\n", (attributes["device-description"]))
		// result := downloadDDL(attributes["device-description"], dcid)
		re := regexp.MustCompile(`esta.sdt\/([\.0-9]+):([0-9]+);.*`)
		res := re.FindSubmatch([]byte(attributes["csl-esta.dmp"]))
		remoteport := fmt.Sprintf("%s", res[2])
		ipaddr := fmt.Sprintf("%s", res[1])
		_, ok := ACNComponents.Load(attributes["cid"])
		if !ok {
			// log.Warnf("NEW CID: %s\n", attributes["cid"])
			component := ACNComponent{
				Addr: ipaddr,
				// DDLFilepath:  result[1],
				CID:          attributes["cid"],
				Port:         remoteport,
				Version:      attributes["version"],
				Model:        attributes["acn-fctn"],
				FriendlyName: attributes["acn-uacn"],
				FirstSeen:    time.Now().Unix(),
				LastSeen:     time.Now().Unix(),
				DCID:         dcid,
				Attributes:   attributes,
			}
			// cid := strings.Replace(strings.ToLower(component.CID), "-", "", 4)
			// log.Printf("\n cid: %v, did:%v\n friendlyname: %v, model: %v\n ipaddr: %v", cid, component.DID, component.FriendlyName, component.Model, ipaddr)
			// go centralapi.ReportComponent(cid, component.DID, component.FriendlyName, component.Model, ipaddr)
			ACNComponents.Store(attributes["cid"], component)
			// log.Warnf("sent component to discover channel\n")
			DiscoverChannel <- component
			// log.Printf("Device DCID: %s\n", dcid)
			// log.Printf("ACNComponents: %+v\n", ACNComponents)
			return
			// } else {
			// 	resource := &ACNComponent{}
			// 	// deepcopier.Copy(component).To(resource)
			// 	cid := Directory Advertisementstrings.Replace(strings.ToLower(resource.CID), "-", "", 4)
			// 	log.Printf("%s\n", cid)
			// 	// go centralapi.UpdateSeenComponent(cid)
			// 	return
		}
		//discoutput := []string{ipaddr, result[1], attributes["cid"], remoteport, flattenDictionary(attributes)}

		// }
	case 5:
		// log.Warnf("Function: Service Ack\n%s\n", data)
	case 6:
		// log.Warnf("Function: Attribute Request\n%s\n", data)
	case 7:
		// log.Warnf("Function: Attribute Reply\n%s\n", data)
	case 8: // DA advertisement
		// log.Warnf("Function: Directory Advertisement\n%s\n", data)
	default:
		log.Errorln("parseSLP unknown")
		log.Errorf("Function: %s\n", slpFunctions[hexToInt(data[1:2])])
		log.Errorf("Packet in question:\n%s", data)
	}
}

// Listen for service registrations over service location protocol to discover acn capable devices and parse the relevant information
func Listen(localAddr string) {
	initLogger()
	addr := net.UDPAddr{
		Port: 427,
		IP:   net.ParseIP(localAddr),
	}
	ser, err := net.ListenUDP("udp", &addr)
	if err != nil {
		log.Errorf("Some error %v\n", err)
		return
	}
	channel := make(chan []byte)
	go listenSRVLOC(ser, channel)
	go loopSLP(ser, addr.IP)

	for {
		x := <-channel
		go parseSLP(x)
		// fmt.Printf("%x\n\n\n", x)
	}
}
