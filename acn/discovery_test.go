package acn

import (
	"rons-magic-software/models"
	"testing"

	"github.com/davecgh/go-spew/spew"
)

func TestParseAttrs(t *testing.T) {
	exampleString := "(cid=F7D59864-17CB-373B-A0BF-E06FDBF7AC09),(acn-fctn=ETC CEM3 Control Module),(acn-uacn=JB),(acn-services=esta.dmp),(csl-esta.dmp=esta.sdt/10.101.101.105:59150;esta.dmp/d:D63B23B8-6407-4E1A-973D-796D777A64B9),(device-description=$:tftp://10.101.101.105/$.ddl),(etc.space=2),(etc.subdevices=499A2C0D-07F3-47FA-8E80-1300BC7D0052:5000000;439C9307-BAF8-4545-8DB9-F03953FF0FF9:101;AACBA029-F87A-4A27-B1BF-62CF00D62351:501;A9A96612-CB87-4591-B181-81742685F1CD:601;A9A96612-CB87-4591-B181-81742685F1CD:611;631444D3-3816-4892-BD43-7C80A5F64294:4000001;C4C07EBD-79C4-4B72-A0F3-386DB288D61A:9000001;04BC7F79-6824-4CFE-9974-82E08496D28A:11000001)"
	got, err := parseAttrs(exampleString)
	if err != nil {
		t.Errorf("TestparseAttrs failed: Function error %s", err)
	}
	want := models.Attributes {
		CID:                  "F7D59864-17CB-373B-A0BF-E06FDBF7AC09",
		DCID:                 "D63B23B8-6407-4E1A-973D-796D777A64B9",
		ACNFctn:              "ETC CEM3 Control Module",
		ACNServices:          "esta.dmp",
		ACNuacn:              "JB",
		CslEstaDmp:           "esta.sdt/10.101.101.105:59150;esta.dmp/d:D63B23B8-6407-4E1A-973D-796D777A64B9",
		CslEstaDmpSubdevices: "",
		EstaSdt:              "10.101.101.105:59150",
		EstaDmp:              "d:D63B23B8-6407-4E1A-973D-796D777A64B9",
		Controller:           false,
		Device:               true,
		DeviceDescription:    "D63B23B8-6407-4E1A-973D-796D777A64B9:tftp://10.101.101.105/D63B23B8-6407-4E1A-973D-796D777A64B9.ddl",
		CslEstaDmpValues:     "",
		ETCSubdevices:        "499A2C0D-07F3-47FA-8E80-1300BC7D0052:5000000;439C9307-BAF8-4545-8DB9-F03953FF0FF9:101;AACBA029-F87A-4A27-B1BF-62CF00D62351:501;A9A96612-CB87-4591-B181-81742685F1CD:601;A9A96612-CB87-4591-B181-81742685F1CD:611;631444D3-3816-4892-BD43-7C80A5F64294:4000001;C4C07EBD-79C4-4B72-A0F3-386DB288D61A:9000001;04BC7F79-6824-4CFE-9974-82E08496D28A:11000001",
		SoftwareVersion:      "",
	}

	if got != want {
		t.Errorf("TestparseSLP failed: wanted, %q, got %q", spew.Sdump(want), spew.Sdump(got))
	}
}
