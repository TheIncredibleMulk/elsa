//go:build windows

package acn

import (
	"os/exec"
	"syscall"
)

// Func to set Windows Specific syscall
func configureCmd(cmd *exec.Cmd) *exec.Cmd {
	cmd.SysProcAttr = &syscall.SysProcAttr{
			HideWindow:    true,
			CreationFlags: 0x08000000,
	}
	return cmd
}

