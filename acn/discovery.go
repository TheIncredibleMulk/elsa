package acn

import (
	"bytes"
	"context"
	"fmt"
	"log/slog"
	"os"
	"os/exec"
	"path/filepath"
	"rons-magic-software/models"
	"runtime"
	"strings"
	"sync"
	"time"

	"github.com/wailsapp/wails/v3/pkg/application"
	"gorm.io/gorm"
)

var slptoolDir = slpOS()

func slpOS() string {
	if runtime.GOOS == "windows" {
		return filepath.Join(os.Getenv("SystemRoot"), "SysWOW64", "slptool.exe")
	}
	return "slptool"
}

// find's slp servers in the area
func Findsrvs(ctx context.Context) (slp []string) {
	slog.Debug("Running Findsrvs")

	cmd := exec.CommandContext(ctx, slptoolDir, "-s", "ACN-DEFAULT", "findsrvs", "service:acn.esta")
	configureCmd(cmd) 
	out, err := cmd.Output()
	if err != nil {
		slog.Error("Findsrvs", "error", err)
		slog.Warn(string(out[:]))
		return nil
	}
	slp = strings.Split(string(out[:]), "\n")
	// spew.Dump(slp)
	slog.Debug("Findsrvs", "slp", slp)
	// TODO: is there always a blank slice item due to line break or is that not guaranteed? if so there is possibility to miss an item
	if len(slp) > 1 {
		return slp[:len(slp)-1]
	} else {
		return nil
	}
}

func Findattrs(ctx context.Context, device string) (attributes models.Attributes, err error) {
	// for _, device := range slpSrvs {
	devUrl := strings.Split(device, ",")
	cmd := exec.CommandContext(ctx, slptoolDir, "-s", "ACN-DEFAULT", "findattrs", devUrl[0])
	configureCmd(cmd)
	out, err := cmd.Output()
	if err != nil {
		slog.Error("Findattrs", "error", err)
		slog.Warn(string(out[:]))
		return models.Attributes{}, err
	}
	if len(out) > 0 {
		out = bytes.TrimRight((out), "\n")
		attributes, err = parseAttrs(string(out))
		if err != nil {
			slog.Error("ParseAttrs", "error", err)
		}
		// attributes = append(attributes, attrs)
	}
	return
}

func parseAttrs(input string) (decoded models.Attributes, err error) {
	for _, attr := range strings.Split(input, ",") {
		attr = strings.Trim(attr, "(")
		attr = strings.Trim(attr, ")")
		attrSplit := strings.Split(attr, "=")
		switch attrSplit[0] {
		case "cid":
			decoded.CID = attrSplit[1]
		case "acn-fctn":
			decoded.ACNFctn = attrSplit[1]
		case "acn-uacn":
			decoded.ACNuacn = attrSplit[1]
		case "acn-services":
			decoded.ACNServices = attrSplit[1]
		case "csl-esta.dmp":
			//special? esta.sdt/10.101.1.93:55687;esta.dmp/cd:F5190BC8-3AF8-4BFA-8661-0DB078971F99
			dmp := strings.Split(attrSplit[1], ";")
			for _, dmpPart := range dmp {
				innerPart := strings.Split(dmpPart, "/")
				switch innerPart[0] {
				case "esta.sdt":
					decoded.EstaSdt = innerPart[1]
				case "esta.dmp":
					decoded.EstaDmp = innerPart[1]
					estaDmpSplit := strings.Split(decoded.EstaDmp, ":")
					decoded.Controller = strings.Contains(estaDmpSplit[0], "c")
					decoded.Device = strings.Contains(estaDmpSplit[0], "d")
					if len(estaDmpSplit) > 1 {
						decoded.DCID = estaDmpSplit[1]
					}
				}
			}

			decoded.CslEstaDmp = attrSplit[1]
		case "device-description":
			//special? need to replace cash signs, protocol is http, ftp, or tftp
			decoded.DeviceDescription = strings.Join(attrSplit[1:], "=")
		case "csl-esta.dmp.values":
			//special?
			decoded.CslEstaDmpValues = attrSplit[1]
		case "version":
			decoded.SoftwareVersion = attrSplit[1]
		case "etc.subdevices":
			//special? DCID:address,DCID:address format
			decoded.ETCSubdevices = attrSplit[1]
		case "csl-esta.dmp.subdevices":
			//same format as etc subdevices
			decoded.CslEstaDmpSubdevices = attrSplit[1]
		}
	}
	decoded.DeviceDescription = strings.Replace(decoded.DeviceDescription, "$", decoded.DCID, -1)
	return
}

func SaveDiscoveredComponent(decoded models.SLPRegistration, location string, db *gorm.DB) error {
	slog.Debug("SaveDiscoveredComponent", "decoded", decoded)
	dec := strings.IndexByte(decoded.EstaSdt, ':')
	if dec == -1 {
		// return fmt.Errorf("[SaveDiscoveredComponent] strings.IndexByte Error %v", decoded)
		return fmt.Errorf("[SaveDiscoveredComponent] Malformed Packet %v", decoded)
	}
	if dec != -1 {
		addr := decoded.EstaSdt[:dec]
		port := decoded.EstaSdt[strings.IndexByte(decoded.EstaSdt, ':')+1:]
		component := models.ACNComponent{
			Addr:         addr,
			DDLFilepath:  decoded.DeviceDescription,
			CID:          strings.ToUpper(decoded.CID),
			Port:         port,
			Version:      decoded.SoftwareVersion,
			Model:        decoded.ACNFctn,
			FriendlyName: decoded.ACNuacn,
			FirstSeen:    time.Now().Unix(),
			LastSeen:     time.Now().Unix(),
			DID:          strings.ToUpper(decoded.DCID),
			Attributes:   strings.Join(decoded.AttributeList, ","),
			Monitor:      true,
			Location:     location,
		}
		// Add to reporter queue if it doesnt exist
		// Add to Local DB
		//HTTP CALL TO AGENT BACKEND
		if component.Exists(db) {
			slog.Info("ACNComponent Exists, skipping creation", "cid", component.CID)
			return nil
		}
		err := component.Create(db)
		app := application.Get()
		app.EmitEvent("acn-new", component.FriendlyName)
		if err != nil {
			return err
		}
		slog.Info("Successfully saved ACN Component", "cid", component.CID)
	}
	return nil
}

func Listen(ctx context.Context, location string, db *gorm.DB) (err error) {
	var cids []string
	app := application.Get()
	for {
		select {
		case <-ctx.Done():
			slog.Warn("Stopping SLP")
			app.EmitEvent("acn-stop", "stop")
			return nil
		default:
			slog.Info("Running SLP")
			
			// Create a context with a 20-second timeout
			timeoutCtx, cancel := context.WithTimeout(ctx, 20*time.Second)
			
			// Create a channel to signal completion
			done := make(chan bool)
			
			// Run the main logic in a separate goroutine
			go func() {
				defer close(done)
				
				cids = Findsrvs(timeoutCtx)
				if cids == nil {
					slog.Warn("No SLP services of specified type found")
			app.EmitEvent("acn-stop", "stop")
					return
				}
				app.EmitEvent("acn-foundCount", len(cids))
				var wg sync.WaitGroup
				resultChan := make(chan struct {
					index int
					attrs models.Attributes
					err   error
				}, len(cids))

				for index, device := range cids {
					wg.Add(1)
					go func(index int, device string) {
						defer wg.Done()
						attrs, err := Findattrs(timeoutCtx, device)
						resultChan <- struct {
							index int
							attrs models.Attributes
							err   error
						}{index, attrs, err}
					}(index, device)
				}

				go func() {
					wg.Wait()
					close(resultChan)
				}()

				for result := range resultChan {
					if result.err != nil {
						slog.Error("Finding attributes", "error", result.err)
						continue
					}

					SLPReg := models.SLPRegistration{
						Attributes: result.attrs,
					}
					err = SaveDiscoveredComponent(SLPReg, location, db)

					if err != nil {
						slog.Error("Saving Component", "error", err)
					}
					app.EmitEvent("acn-new", fmt.Sprintf("%d: %s", result.index, result.attrs.ACNFctn))
				}

				app.EmitEvent("acn-foundCount", len(cids))

				app.EmitEvent("acn-stop", "stop")
				done <- true
			}()

			// Wait for either completion or timeout
			select {
			case <-done:
				slog.Info("SLP completed successfully")
			case <-timeoutCtx.Done():
				slog.Warn("SLP timed out after 20 seconds")
			}

			// Clean up
			cancel()

			// Sleep for 3 minutes before the next iteration
			time.Sleep(180 * time.Second)
		}
	}
}

